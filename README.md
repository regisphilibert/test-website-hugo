To build this site with Hugo first you will need to add the ananke theme, enter the following in to your terminal/command prompt window:

    git submodule add https://github.com/budparr/gohugo-theme-ananke.git themes/ananke
    echo 'theme = "ananke"' >> config.toml
    hugo
